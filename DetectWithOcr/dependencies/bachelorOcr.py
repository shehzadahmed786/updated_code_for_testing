from .utils.read_json import data_in_json
import glob
import cv2
import json
import os
import pandas as pd
import sys
import argparse
from pathlib import Path 

FILE = Path(__file__).resolve()
# DATA = PureWindowsPath
ROOT = FILE.parents[0] 
if str(ROOT) not in sys.path:
    sys.path.append(str(ROOT))  # add ROOT to PATH
ROOT = Path(os.path.relpath(ROOT, Path.cwd()))  # relative
# USed for acquire old result by proving exp unique IDs 
def all_high_school(
    ocr
    ):
    uni=ocr
    arr = []
    dic=dict()
    # print("something", highocr)    
    # try:
    for i in glob.glob(uni + "/*"):
        path = i + '/'
        # print("path ",path)
        for j in glob.glob(path + "*.jpg"):
            arr.append(j)
    for array in arr:
        # array.replace("\\","/")
        # array.replace("/","\\")
        data =array.split("/")

        # print("what array ", data)
        dic[data[-2]]=cv2.imread(array)
        # print("",dic[data[-1]]=cv2.imread(array))
    result = data_in_json(dic, ocr)
    # print("what are the keys ==== ", dic.keys())
    # print("what is result ", len(result))
    if len(result) == 2:
        print("result not found")
    else:
        # print("what is result ", result)
        with open("High_School_ocr.json", "w") as outfile:
            json.dump(result, outfile)
        dictionary = json.dumps(result)
        # print("File Found")    
        return dictionary
    # except:
    #     print("File Not Found")
    
    
def parse_opt():
    parser = argparse.ArgumentParser()
    parser.add_argument('--ocr', type=str, help='file/dir/URL/glob, 0 for webcam')
    # parser.add_argument('--uniName', type=str, help='file/dir/URL/glob, 0 for webcam')
    opt = parser.parse_args()
    return opt

def main(opt):

    all_high_school(**vars(opt))

if __name__ == "__main__":
    opt = parse_opt()
    main(opt)