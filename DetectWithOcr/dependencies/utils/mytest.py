import cv2
import numpy as np


def main(img):
    height, width = img.shape[:2]
    center = (width/2, height/2)
    rotate_matrix = cv2.getRotationMatrix2D(center=center, angle=90, scale=1)
    rotated_image = cv2.warpAffine(src=img, M=rotate_matrix, dsize=(width, height))
    return rotated_image

def rotate_image(image, angle):
  image_center = tuple(np.array(image.shape[1::-1]) / 2)
  rot_mat = cv2.getRotationMatrix2D(image_center, angle, 1.0)
  result = cv2.warpAffine(image, rot_mat, image.shape[1::-1], flags=cv2.INTER_LINEAR)
  return result
def myfuc(img):
    

    print(img.shape)
    img_rotate_90_clockwise = cv2.rotate(img, cv2.ROTATE_90_CLOCKWISE)
    return img_rotate_90_clockwise
    # img_rotate_90_counterclockwise = cv2.rotate(img, cv2.ROTATE_90_COUNTERCLOCKWISE)
    # cv2.imwrite('data/dst/lena_cv_rotate_90_counterclockwise.jpg', img_rotate_90_counterclockwise)
    # img_rotate_180 = cv2.rotate(img, cv2.ROTATE_180)
    # cv2.imwrite('data/dst/lena_cv_rotate_180.jpg', img_rotate_180)

img="C:/Users/Hassan/Downloads/data/KING_ABDULAZIZ_UNIVERSITY/KING_ABDULAZIZ_UNIVERSITY_7.pdf_output/id/1.jpg"
img=cv2.imread(img)
for i in range(4):
    img=myfuc(img)
    cv2.imshow("image", img)
    cv2.waitKey(0)
