import json
from sys import flags
import easyocr
import numpy as np   
import fitz
import glob
import cv2
# import json
# def isint(num):
#     d=num
#     splt = d.split(",")    
#     if splt:
#         d = d.replace(',', '.')
#     try:
#         if float(d):
#             print("float")
#             return float(d)
#         else:
#             d = int(d)
#             return d
#     except:
#         return "no_number"
# def data_in_json(data_dic):
#     reader = easyocr.Reader(['en'])
#     text = dict()
#     for k,v in data_dic.items():
#         dat = v
#         try:
#             resul = reader.readtext(dat,detail = 0)
#             if k == "Identity" or k == "identity":
#                 continue
#             elif k == "CGP" and len(resul) == 2 or len(resul) == 3 or len(resul) == 4:
#                 d = isint(resul[0])
#                 if d == "no_number":
#                     d1 = isint(resul[1])
#                     if d1 == "no_number":
#                         text[k] = resul[2]
#                     else:
#                         text[k] = resul[1]
#                 else:
#                     text[k] = d
#             elif k == "CAA" and len(resul) == 2 or len(resul) == 3 or len(resul) == 4:
#                 d = isint(resul[0])
#                 if d == "no_number":
#                     d1 = isint(resul[1])
#                     if d1 == "no_number":
#                         text[k] = resul[2]
#                     else:
#                         text[k] = resul[1]
#                 else:
#                     text[k] = d
#             else:
#                 text[k] = resul[0]
#         except:
#             print("Image Quality Not good")
#     with open("HighSchool.json", "w") as outfile:
#         json.dump(text, outfile)
#     dictionary = json.dumps(text)
#     print(dictionary)
#     return dictionary

def isint(num):
    d=num
    splt = d.split(",")
    if splt:
        d = d.replace(',', '.')
    try:
        if float(d):
            return float(d)
        else:
            d = int(d)
            return d
    except:
        return "no_number"

def pdf_to_images(pdf_path):
    global myoutput
    try:    
        pdffile =glob.glob(pdf_path)
        zoom = 2    # zoom factor
        mat = fitz.Matrix(zoom, zoom)
        if (pdffile):
            count = 0
            for pdf in pdffile:
                doc = fitz.open(pdf)
                page = doc.load_page(page_id=0)  # number of page
                # print("what is pdf ", page)
                pix = page.get_pixmap(matrix = mat)
                count += 1
                output = "data/images/"+str(count)+".png"
                pix.save(output)
                
                return "Yes"
        else:
            return "No"
    except:
        print("Path Not Correct")
# used for converting the crop image into text by using OCR 
def data_in_json(data_dic):
    import re
    global myoutput
    # print("what is data dictionary ", data_dic)
    reader = easyocr.Reader(['en'])
    text = dict()
    kernel = np.array([[0, -1, 0],
                   [-1, 5,-1],
                   [0, -1, 0]])
    # # print("what is k ", data_dic.keys() ," ====== v ====== ")
    flags=True
    for k,v in data_dic.items():
        dat = v
        # print("what are the keys ==== ", k.lower())
        result=''
        try:
            # print("what are the keys ", k.lower())
            # image_improve1 = cv2.filter2D(src=dat , ddepth=-1 , kernel = kernel)
            # ret,thresh1 = cv2.threshold(image_improve1,200,255,cv2.THRESH_BINARY_INV)
            # ret,thresh1 = cv2.threshold(thresh1,200,255,cv2.THRESH_BINARY_INV)
            # again_crp = thresh1[13: , :]
            resul = reader.readtext(dat,detail = 0)
            # print("what is result ====== ocr =====  in try ", resul)
            # print(resul,"reding ocr :", k)
            if k == "Identity" or k == "identity":
                continue    
            elif k.lower()=="cgp" or k.lower() == "gpa" or k.lower() == "caa":
                k="GPA"
                # print("resul ===== in CGP ==== ", resul)
                if flags:
                    result="".join(resul)
                    result=result.strip("")
                    res=re.findall(r'\d+\.\d+',result)
                    # print("resul ===== in CGP after flag ==== ", resul)

                    if res:
                        text[k]=res[0]
                        flags=False
                        continue
                    else :
                        res=re.findall(r'\d+',result)
                        if res:
                            if len(res) <= 3:
                                myresult=res[0]+"."+res[1]
                                text[k] = myresult

            if  k.lower() == "cgpa" and flags:
                result="".join(resul)
                result=result.strip("")
                res=re.findall(r'\d+\.\d+',result)
                # print(res,"In gCpa detection")
                if res:
                    text[k]=res[0]    
                    continue
            if k.lower()=='id':
                k='ID'
                image_improve = cv2.filter2D(src=dat , ddepth=-1 , kernel = kernel)
                ret,thresh2 = cv2.threshold(image_improve,127,255,cv2.THRESH_BINARY_INV)
                ret,thresh2 = cv2.threshold(thresh2,127,255,cv2.THRESH_BINARY_INV)
                
                # ret,thresh2 = cv2.threshold(thresh2,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,\
                # cv2.THRESH_BINARY,11,2)
                # crop_again = thresh2[10: , 40:]
                result1 = reader.readtext(thresh2, detail=0)
                # print('what is result1 ====== ocr ==== ', result1)
                result="".join(result1)
                res=re.findall(r"\d+",result)
                # print('what is result1 ====== regex ocr  ==== ', res)

                # print("in ID ==== ")
                if res:
                    if len(res)>0:
                        if len(res[0])>=3:
                            text[k]=res[0]
                            continue 
        except Exception as e:
            print("Image Quality Not good")
    with open("output.json", "w") as outfile:
        json.dump(text, outfile)
    with open("alldata.txt","a") as a:
        dictionary = json.dumps(text)
        # a.writelines(str(str(dictionary)+","+str(pdfpath)+"\n"))    
    print(dictionary)
    return dictionary
