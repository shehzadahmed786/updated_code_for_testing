from unittest.util import _MAX_LENGTH
import cv2
from cv2 import split
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sn
import glob
import easyocr
import json
import re
import datetime
from datetime import date
import re

# def myremarks(gpa,outofgpa):
#     percentage=gpa/outofgpa*100
#     if percentage>=90:
#         remarks="Excellent"
#     elif percentage>=80:
#         remarks="Very Good"
#     elif percentage>=70:
#         remarks="Good"
#     elif percentage>=60:
#         remarks="Average"
#     elif percentage>=50:
#         remarks="Pass"
#     else:
#         remarks="Fail"
#     return remarks


def rotateimg(img):

    # image = cv2.imread(img)
    img_rotate_90_clockwise = cv2.rotate(img, cv2.ROTATE_90_CLOCKWISE)

    return img_rotate_90_clockwise
    
def ocrtext(img):
    reader = easyocr.Reader(['en'])
    # text = dict()
    text= reader.readtext(img, detail =0)
    return text


# used for converting the crop image into text by using OCR 
def data_in_json(data_dic, path):
    myresult = dict()
    

    kernel = np.array([[0, -1, 0],
                   [-1, 5,-1],
                   [0, -1, 0]])
    for k,v in data_dic.items():
        dat = v
        k=str(k).lower()

        # try:
        if k=='uni_name' or k == "last_name" or k =="stamp":
            continue
        if k=='identity':
            # identity = text=ocrtext(dat)
            # print(identity,"iden============================")
            # x = re.findall('[A-Z]+', str(identity))
            x = "STEP"
            myresult['IDENTITY']=x
         
        if k=='id':
            t = text=ocrtext(dat)
            # print(t,'id====================================')
            x = re.findall('[0-9]+', str(t))
            x=" ".join(x)
            myresult['ID']=x
            
        
        if k=='marks':
            mark = text=ocrtext(dat)
            print(mark,"===============================marksss")
            # print(mark)
            x = re.findall('\d+', str(mark))
            x=" ".join(x)
            myresult['MARK']=x
            # print("marks======================",x)
        

        if k=='date':
            text=ocrtext(dat)
            if len(text)>=1:
                myresult['DATE']=text
        if k=='date_h':
                text=ocrtext(dat)
                myresult['DATE']=text
                print(text,'========================================new data')
                
           

    
    with open("Step_out.json", "w") as outfile:
        json.dump(myresult, outfile)
    dictionary = json.dumps(myresult)
    with open("Step.txt","a") as a:
        dictionary = json.dumps(myresult)
        a.writelines(str(str(dictionary)+","+str(path)+"\n")) 
    print(dictionary)
    return dictionary

    