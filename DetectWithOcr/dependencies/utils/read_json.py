import cv2
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sn
import glob
import easyocr
import json
import re


def myremarks(gpa,outofgpa):
    percentage=gpa/outofgpa*100
    if percentage>=90:
        remarks="Excellent"
    elif percentage>=80:
        remarks="Very Good"
    elif percentage>=70:
        remarks="Good"
    elif percentage>=60:
        remarks="Average"
    elif percentage>=50:
        remarks="Pass"
    else:
        remarks="Accepted"
    return remarks


def rotateimg(img):

    # image = cv2.imread(img)
    img_rotate_90_clockwise = cv2.rotate(img, cv2.ROTATE_90_CLOCKWISE)

    return img_rotate_90_clockwise
    
def ocrtext(img):
    reader = easyocr.Reader(['en'])
    text= reader.readtext(img, detail =0)
    return text


# used for converting the crop image into text by using OCR 
def data_in_json(data_dic, path):
    myresult = dict()
    

    kernel = np.array([[0, -1, 0],
                   [-1, 5,-1],
                   [0, -1, 0]])
    for k,v in data_dic.items():
        dat = v
        k=str(k).lower()

        # try:
        if k=='uni_name' or k == "identity" or k =="stamp":
            continue

        if k == 'cpa' or k=="gpa" or k=="cgpa":
            
            myresult["OUTOFGPA"]="4" if k=="cgpa" else "5" 
            k='GPA'
                
            ret,thresh2 = cv2.threshold(dat,100,255,cv2.THRESH_BINARY_INV)
            # ret,thresh2 = cv2.threshold(thresh2,150,255,cv2.THRESH_BINARY_INV)
            
            image_sharp = cv2.filter2D(src=thresh2, ddepth=-1, kernel=kernel)

            cropped_image = cv2.detailEnhance(image_sharp)
            # cv2.imwrite("img.png",cropped_image)

            text=ocrtext(cropped_image)
            print(text,"text")

            counter=0
            while  counter<4:
                print("GPA loop",counter)
                text="".join(text)
                text=text.replace(" ","")
                # print("text",text)
                # value=re.findall(r'\d\.\d+',text)
                value=re.findall(r'[0-9]+\.[0-9]{1,2}',text)
                
                print(text,"=====================text============")
                print(value,"=============value========")
                
                # print("value",(value))
                counter+=1
                if len(value)==0:
                    result=re.findall(r'\d+',text)
                    result=list(filter(lambda x:len(str(x))==3,result))
                    if result:
                        value=str(result[0]) if result is not None else 0
                        if value and int(value[:1])<6:
                            value=value[0:1]+"."+value[1:]
                            myresult[k]=value
                            myresult["GRADE"]=myremarks(float(value),float(myresult["OUTOFGPA"]))
                            
                            break
                        
                else:
                    # print(value,"value")
                    myresult[k]=value[0]
                    myresult["GRADE"]=myremarks(float(value[0]),float(myresult["OUTOFGPA"]))
                    break
                cropped_image = rotateimg(cropped_image )

                text=ocrtext(cropped_image)


        if k=="id" or k=='uni_id':  
            k='ID'
            
            ret,thresh2 = cv2.threshold(dat,100,255,cv2.THRESH_BINARY_INV)
            ret,thresh2 = cv2.threshold(thresh2,150,255,cv2.THRESH_BINARY_INV)
            
            image_sharp = cv2.filter2D(src=thresh2, ddepth=-1, kernel=kernel)

            cropped_image = cv2.detailEnhance(image_sharp)
            cv2.imwrite("img.jpg",cropped_image)

            text=ocrtext(cropped_image)
            text="".join(text)
            value=re.findall('\d+',text)
            value="".join(value)
            # print(value,"text")
            counter=0
            
            while len(value)<9:
                cropped_image= rotateimg(cropped_image)
                text=ocrtext(cropped_image) 
                text="".join(text)
                value=re.findall('\d+',text)
                value="".join(value)
                # print(value,"text")
                if len(value)>9 or counter>=4:
                    break
                counter+=1
                
            # print(value,"text")
            counter=0
            if len(value)>4:
                myresult[k]=value[1:]
            else:
                myresult[k]=value
            
        
    
    with open("bachelorOcr_out.json", "w") as outfile:
        json.dump(myresult, outfile)
    dictionary = json.dumps(myresult)
    with open("Bech.txt","a") as a:
        dictionary = json.dumps(myresult)
        a.writelines(str(str(dictionary)+","+str(path)+"\n"))    
    
    print(dictionary)
    return dictionary