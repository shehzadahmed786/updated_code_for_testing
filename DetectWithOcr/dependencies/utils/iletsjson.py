from unittest.util import _MAX_LENGTH
import cv2
from cv2 import split
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sn
import glob
import easyocr
import json
import re


# def myremarks(gpa,outofgpa):
#     percentage=gpa/outofgpa*100
#     if percentage>=90:
#         remarks="Excellent"
#     elif percentage>=80:
#         remarks="Very Good"
#     elif percentage>=70:
#         remarks="Good"
#     elif percentage>=60:
#         remarks="Average"
#     elif percentage>=50:
#         remarks="Pass"
#     else:
#         remarks="Fail"
#     return remarks


def rotateimg(img):

    # image = cv2.imread(img)
    img_rotate_90_clockwise = cv2.rotate(img, cv2.ROTATE_90_CLOCKWISE)

    return img_rotate_90_clockwise
    
def ocrtext(img):
    reader = easyocr.Reader(['en'])
    text = reader.readtext(img, detail =0)

    return text


# used for converting the crop image into text by using OCR 
def data_in_json(data_dic, path):
    myresult = dict()
    

    kernel = np.array([[0, -1, 0],
                   [-1, 5,-1],
                   [0, -1, 0]])
    for k,v in data_dic.items():
        dat = v
        k=str(k).lower()

        # try:
        if k=='uni_name' or k == "last_name" or k =="stamp":
            continue
        if k=='first_name':
            first_name = text=ocrtext(dat)
            print(first_name)
            x = re.findall('[A-Z]+', str(first_name))
            # print(x,"firstname")
            if len(x)>0:
                x=[i for i in x if len(i)>1]
                x=" ".join(x)
                myresult['FIRST_NAME']=x
        if k=='type':
            t = text=ocrtext(dat)
            print(t)
            x = re.findall('[A-Z]+', str(t))
            x=" ".join(x)
            myresult['TYPE']=x

        if k=='family_name':
            fm_name = text=ocrtext(dat)
            # print(fm_name[-1])
            # myresult[k]=fm_name[-1]
            x = re.findall('[A-Z]+', str(fm_name))
            # print(x,"FAMILY_NAME")
            if len(x)>0:
                x=[i for i in x if len(i)>1]
                print(x)
                x=" ".join(x)
                myresult['FAMILY_NAME']=x
        
        if k=='id':
            ide = text=ocrtext(dat)
            x = re.findall('[A-Z + 0-9+]+', str(ide))
            print(x)
            x=[i for i in x if len(i)>4]
            x = " ".join(x)
            # print(x,"ID=======================")
            myresult['ID']=x


            # if len(x)>0:
            #     x=[i for i in x if len(i)>3]
            #     print(x,"Join=========================")
            #     myresult['ID']=x
           
        if k=='trf_name':
            text=ocrtext(dat)
            x = re.findall('[A-Z + 0-9+]+', str(text))
            # print(x,"TRF============================")
      
            x=[i for i in x if len(i)>11]
            x=" ".join(x)
            myresult['TRF_NUMBER']=x
                
            # value=re.findall(r'\d\.\d+',text)
            # print(text[-2])
            # myresult[k]=text[-2]

        if k=='total_marks':
            fe=ocrtext(dat)
            print(fe,'=======fe')
            ch = re.findall("^[0-9.-]*$",str(fe))
            ch=ch[0] if len(ch)>0 else ""
            if len(ch)==0:
                ch=re.findall("\d+",str(fe))
                ch ="".join(ch)
                ch=ch[:1]+"."+ch[1:]
                

            print("ch================",ch)
            myresult["BAND_SCORE"]=ch
            
    
    with open("Ielts_out.json", "w") as outfile:
        json.dump(myresult, outfile)
    # dictionary = json.dumps(myresult)
    with open("Ilets.txt","a") as a:

        dictionary = json.dumps(myresult)
        a.writelines(str(str(dictionary)+","+str(path)+"\n")) 
    print(dictionary)
    return dictionary
