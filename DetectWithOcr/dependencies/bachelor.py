
import argparse
import os
import sys
from pathlib import Path
from unicodedata import name
import torch
import torch.backends.cudnn as cudnn
from PIL import ImageEnhance


FILE = Path(__file__).resolve()

ROOT = FILE.parents[0]  # YOLOv5 root directory
if str(ROOT) not in sys.path:
    sys.path.append(str(ROOT))  # add ROOT to PATH
ROOT = Path(os.path.relpath(ROOT, Path.cwd()))  # relative

from models.common import DetectMultiBackend
from utils.datasets import IMG_FORMATS, VID_FORMATS, LoadImages, LoadStreams
from utils.general import (LOGGER, check_file, check_img_size, check_imshow, check_requirements, colorstr, cv2,
                           increment_path, non_max_suppression, print_args, scale_coords, strip_optimizer, xyxy2xywh)
from utils.plots import Annotator, colors, save_one_box , pdf_to_images , data_in_json
from utils.torch_utils import select_device, time_sync
import glob
import cv2
import numpy as np
import json
import fitz
import pandas as pd
import datetime
import csv
from PIL import Image, ImageEnhance

  
@torch.no_grad()

def run(
    
King_abdulaziz_university='',
Qasim_university='',
King_faisal_university='',
king_khalid_university='',
king_saud_university='',
Saudi_electronic_university='',
Taibah_university='',
Taif_university='',
Umm_al_qura_university='',
Imam_Muhammad_University='',
Jazan_university='',
King_saud_health_science='',
Princess_Nora='',
highschool='',
Ielts='',
Step='',
Steps=''
):



    p=os.path.abspath(os.getcwd())
    configrations={
        "King_abdulaziz_university":[ROOT / 'weights/king_abdulaziz_university.pt', ROOT /'yamls/King_abdulaziz_university.yaml' ],
        "Qasim_university":[ROOT / 'weights/QU.pt', ROOT / 'yamls/QU.yaml' ],
        "King_faisal_university":[ROOT / 'weights/king_faisal_university.pt', ROOT / 'yamls/king_faisal_university.yaml' ],
        "king_khalid_university":[ROOT / 'weights/king_khalid_university.pt', ROOT / 'yamls/king_khalid_university.yaml' ],
        "king_saud_university":[ROOT / 'weights/King_Saud_University_without_ID_best.pt', ROOT / 'yamls/King_Saud_University_without_ID_data.yaml' ],
        "Saudi_electronic_university":[ROOT / 'weights/Saudi_Electronic_University.pt', ROOT / 'yamls/Saudi_Electronic_University.yaml' ],
        "Taibah_university":[ROOT / 'weights/TAIBAH_UNIVERSITY.pt', ROOT / 'yamls/TAIBAH_UNIVERSITY.yaml' ],
        "Taif_university":[ROOT / 'weights/taif_university.pt', ROOT / 'yamls/taif_university.yaml' ],
        "Umm_al_qura_university":[ROOT / 'weights/umm_al-qura_university.pt', ROOT / 'yamls/umm_al-qura_university.yaml' ],
        "Jazan_university":[ROOT / 'weights/JAZAN_UNIVERSITY.pt', ROOT / 'yamls/JAZAN_UNIVERSITY.yaml' ],
        "Imam_Muhammad_University":[ROOT / 'weights/imam_muhammad_islamic_univ.pt', ROOT / 'yamls/imam_muhammad_islamic_univ.yaml' ],
        "King_saud_health_science":[ROOT / 'weights/K_SAUD_UNI_FOR_HEALTH_SCIENCES.pt', ROOT / 'yamls/K_SAUD_UNI_FOR_HEALTH_SCIENCES.yaml' ],
        "Princess_Nora":[ROOT / 'weights/Princess_Nora_University.pt', ROOT / 'yamls/Princess_Nora_University.yaml'],
        "highschool":[ROOT / 'weights/highschool.pt', ROOT / 'yamls/highschool.yaml'],        
        "Ielts":[ROOT / 'weights/Ilets.pt', ROOT / 'yamls/Ilets.yaml'],
        "Step":[ROOT / 'weights/Step.pt', ROOT / 'yamls/Step.yaml'],        
        "Steps":[ROOT / 'weights/Step.pt', ROOT / 'yamls/Step.yaml'],        

    }

    university={
    
        "King_abdulaziz_university":King_abdulaziz_university,
        "King_faisal_university":King_faisal_university,
        "king_khalid_university":king_khalid_university,
        "king_saud_university":king_saud_university,
        "Saudi_electronic_university":Saudi_electronic_university,
        "Taibah_university":Taibah_university,
        "Taif_university":Taif_university,
        'Qasim_university':Qasim_university,
        'Umm_al_qura_university':Umm_al_qura_university,
        'Jazan_university':Jazan_university,
        "Imam_Muhammad_University":Imam_Muhammad_University, 
        "King_saud_health_science":King_saud_health_science, 
        "Princess_Nora":Princess_Nora,
        "highschool":highschool,
        "Ielts":Ielts,
        "Step":Step,
        "Steps":Steps


        }
    path=''
    universityName=''
    # key = ""
    k = ""
    for k, v in university.items():
        k = k
        # print("what is key in university ", k , " ===== and V ",v )
        if university[k] is not None:
            master=v
            uni=k
            break
    weights=configrations[k][0] # model.pt path(s)
    source=ROOT / 'data/images'  # file/dir/URL/glob, 0 for webcam # file/dir/URL/glob, 0 for webcam
    data=str(configrations[k][1]).replace("\\",'/')  # dataset.yaml path

    print(weights,"=======")
    print(data,"=======")
    imgsz=(640, 640)  # inference size (height, width)
    conf_thres=0.25  # confidence threshold
    iou_thres=0.45  # NMS IOU threshold
    max_det=1000  # maximum detections per image
    device=''  # cuda device, i.e. 0 or 0,1,2,3 or cpu
    view_img=False  # show results
    save_txt=False  # save results to *.txt
    save_conf=False  # save confidences in --save-txt labels
    save_crop=True  # save cropped prediction boxes
    nosave=False  # do not save images/videos
    classes=None  # filter by class: --class 0, or --class 0 2 3
    agnostic_nms=False  # class-agnostic NMS
    augment=False  # augmented inference
    visualize=False  # visualize features
    update=False  # update all models
    project=ROOT / 'runs/detect/crops'  # save results to project/name
    # name='exp',  # save results to project/name
    exist_ok=False  # existing project/name ok, do not increment
    line_thickness=1  # bounding box thickness (pixels)
    hide_labels=False  # hide labels
    hide_conf=True  # hide confidences
    half=False  # use FP16 half-precision inference
    dnn=True  # use OpenCV DNN for ONNX inference




         

    
    pdf = pdf_to_images(master,ROOT)
    print(master,"path")
    if pdf == "Yes":
        master = str(master)+ "_output"
        source = str(source) 
        save_img = not nosave and not source.endswith('.txt')  # save inference images
        is_file = Path(source).suffix[1:] in (IMG_FORMATS + VID_FORMATS)
        is_url = source.lower().startswith(('rtsp://', 'rtmp://', 'http://', 'https://'))
        webcam = source.isnumeric() or source.endswith('.txt') or (is_url and not is_file)
        if is_url and is_file:
            source = check_file(source)  # download
        # Directories
        save_dir = increment_path(Path(master), exist_ok=exist_ok)  # increment run
        (save_dir / 'labels' if save_txt else save_dir).mkdir(parents=True, exist_ok=True)  # make dir
        # Load model
        device = select_device(device)
        model = DetectMultiBackend(weights, device=device, dnn=dnn, data=data, fp16=half)
        stride, names, pt = model.stride, model.names, model.pt
        imgsz = check_img_size(imgsz, s=stride)  # check image size
        # Dataloader
        if webcam:
            view_img = check_imshow()
            cudnn.benchmark = True  # set True to speed up constant image size inference
            dataset = LoadStreams(source, img_size=imgsz, stride=stride, auto=pt)
            bs = len(dataset)  # batch_size
        else:
            dataset = LoadImages(source, img_size=imgsz, stride=stride, auto=pt)
            # print("what is in dataset ", dataset)
            bs = 1  # batch_size
        vid_path, vid_writer = [None] * bs, [None] * bs
        
        # Run inference
        dlabels = list()
        data_dic = dict()

        model.warmup(imgsz=(1 if pt else bs, 3, *imgsz))  # warmup
        dt, seen = [0.0, 0.0, 0.0], 0
        for path, im, im0s, vid_cap, s in dataset:
            # print("path ", im , "type = ", type(im))
            t1 = time_sync()
            im = torch.from_numpy(im).to(device)
            im = im.half() if model.fp16 else im.float()  # uint8 to fp16/32
            im /= 255  # 0 - 255 to 0.0 - 1.0
            if len(im.shape) == 3:
                im = im[None]  # expand for batch dim
            t2 = time_sync()
            dt[0] += t2 - t1

            # Inference
            visualize = increment_path(save_dir / Path(path).stem, mkdir=True) if visualize else False
            pred = model(im, augment=augment, visualize=visualize)
            t3 = time_sync()
            dt[1] += t3 - t2
            # NMS
            pred = non_max_suppression(pred, conf_thres, iou_thres, classes, agnostic_nms, max_det=max_det)
            dt[2] += time_sync() - t3

            # Process predictions
            for i, det in enumerate(pred):  # per image
                seen += 1
                if webcam:  # batch_size >= 1
                    p, im0, frame = path[i], im0s[i].copy(), dataset.count
                    s += f'{i}: '
                else:
                    p, im0, frame = path, im0s.copy(), getattr(dataset, 'frame', 0)

                p = Path(p)  # to Path
                save_path = str(save_dir / str(master) / p.name)  # im.jpg
                txt_path = str(save_dir / 'labels' / p.stem) + ('' if dataset.mode == 'image' else f'_{frame}')  # im.txt
                s += '%gx%g ' % im.shape[2:]  # print string
                gn = torch.tensor(im0.shape)[[1, 0, 1, 0]]  # normalization gain whwh
                im0 = cv2.detailEnhance(im0)
                # im0 = cv2.threshold(im0,150,255,cv2.THRESH_BINARY_INV)
                imc = im0.copy() if save_crop else im0  # for save_crop
                           
                annotator = Annotator(im0, line_width=line_thickness, example=str(names))
                if len(det):
                    # Rescale boxes from img_size to im0 size
                    det[:, :4] = scale_coords(im.shape[2:], det[:, :4], im0.shape).round()

                    # Print results
                    for c in det[:, -1].unique():
                        n = (det[:, -1] == c).sum()  # detections per class
                        s += f"{n} {names[int(c)]}{'s' * (n > 1)}, "  # add to string

                    # Write results
                    for *xyxy, conf, cls in reversed(det):
                        if save_txt:  # Write to file
                            xywh = (xyxy2xywh(torch.tensor(xyxy).view(1, 4)) / gn).view(-1).tolist()  # normalized xywh
                            line = (cls, *xywh, conf) if save_conf else (cls, *xywh)  # label format
                            with open(txt_path + '.txt', 'a') as f:
                                f.write(('%g ' * len(line)).rstrip() % line + '\n')

                        c = int(cls)  # integer class
                        
                        if save_img or save_crop or view_img:  # Add bbox to image
                            label = None if hide_labels else (names[c] if hide_conf else f'{names[c]} {conf:.2f}')
                            # if names[c]=="family_name" or names[c]=="first_name" or names[c]=="id" or names[c]=="total_marks" or names[c]=="trf_name" and Ilets is not None:
                                # imc2 = imc
                                # im1 = im0
                                
                            if names[c]=="gpa" and Umm_al_qura_university is not None:
                                names[c]='cgpa'
                            elif names[c]=="cgpa" and Taibah_university is not None:
                                names[c]="gpa"
                            annotator.box_label(xyxy, label, color=colors(c, True))
                            if save_crop:

                                
                                
                                ret,imc = cv2.threshold(imc,180,255,cv2.THRESH_BINARY_INV)


                                
                                
                                ret,thresh2 = cv2.threshold(imc,127, 255, cv2.THRESH_TOZERO)
                               
                                
                            
                                enhance=cv2.detailEnhance(thresh2)
                                
                                if Ielts is not None:
                                    # ret,imc = cv2.threshold(imc,180, 255, cv2.THRESH_TOZERO)
                                
                                    result= save_one_box(xyxy, im0, file=save_dir / str(master) /  names[c] / f'{p.stem}.jpg', BGR=True)
                                
                                else:
                                    
                                    
                                    result= save_one_box(xyxy, thresh2, file=save_dir / str(master) /  names[c] / f'{p.stem}.jpg', BGR=True)
                                dlabels.append(names[c])
                im0 = annotator.result()
           
                if save_img:
                    if dataset.mode == 'image':
                       
                        cv2.imwrite(save_path, im0)
                        # cv2.imwrite(save_path, im1)

    
        lab =set(dlabels)
        
        if len(lab) >=3:
            print("template Match")
            print('True')
            dictionary ={
                "Template_Matched" : "True",
                }
            # Serializing json 
            json_object = json.dumps(dictionary, indent = 4)
            # Writing to sample.json

            # with open("Template.json", "w") as outfile:
            #     outfile.write(json_object)
        else:
            if king_saud_university is not None and len(lab)==2:
                print("template Match")
                print('True')
            elif Umm_al_qura_university is not None and len(lab)==2:    
                print("template Match")
                print('True')
            elif Taif_university is not None and len(lab)==2:
                print("template Match")
                print('True')
            elif Ielts is not None and len(lab)==5:
                print("template Match")
                print("True")
            elif Step is not None and len(lab)==5:
                print("template Match")
                print("True")
            elif king_saud_university is not None and len(lab)==2:
                print("template Match")
                print("True")
            else:
                print("Template Not Match"+' '+'False')
                print("But Your file haxs been uploaded")
    else:
        print("File Not Found or nor in correct format (.pdf)")
   

