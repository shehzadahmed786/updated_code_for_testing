from dependencies import bachelor,bachelorOcr
import argparse
from dependencies.utils.general import LOGGER,check_requirements,print_args
from pathlib import Path
import os,sys

FILE = Path(__file__).resolve()
ROOT = FILE.parents[0]  # YOLOv5 root directory
if str(ROOT) not in sys.path:
    sys.path.append(str(ROOT))  # add ROOT to PATH
ROOT = Path(os.path.relpath(ROOT, Path.cwd())) 


def parse_opt():
    parser = argparse.ArgumentParser()
    parser.add_argument('--Qasim_university', type=str,  help='file/dir/URL/glob, 0 for webcam')
    parser.add_argument('--King_abdulaziz_university', type=str, help='file/dir/URL/glob, 0 for webcam')
    parser.add_argument('--King_faisal_university', type=str, help='file/dir/URL/glob, 0 for webcam')
    parser.add_argument('--king_khalid_university', type=str, help='file/dir/URL/glob, 0 for webcam')
    parser.add_argument('--king_saud_university', type=str, help='file/dir/URL/glob, 0 for webcam')
    parser.add_argument('--Saudi_electronic_university', type=str, help='file/dir/URL/glob, 0 for webcam')
    parser.add_argument('--Taibah_university', type=str, help='file/dir/URL/glob, 0 for webcam')
    parser.add_argument('--Taif_university', type=str, help='file/dir/URL/glob, 0 for webcam')
    parser.add_argument('--Umm_al_qura_university', type=str, help='file/dir/URL/glob, 0 for webcam')
    parser.add_argument('--Jazan_university', type=str, help='file/dir/URL/glob, 0 for webcam')
    parser.add_argument('--Imam_Muhammad_University', type=str, help='file/dir/URL/glob, 0 for webcam')
    parser.add_argument('--King_saud_health_science', type=str, help='file/dir/URL/glob, 0 for webcam')
    parser.add_argument('--Princess_Nora', type=str, help='file/dir/URL/glob, 0 for webcam')
    parser.add_argument('--highschool', type=str, help='file/dir/URL/glob, 0 for webcam')
    parser.add_argument('--Ielts', type=str, help='file/dir/URL/glob, 0 for webcam')
    parser.add_argument('--Step', type=str, help='file/dir/URL/glob, 0 for webcam')
    parser.add_argument('--Steps', type=str, help='file/dir/URL/glob, 0 for webcam')
    
    # parser.add_argument('--root', type=str, help='file/dir/URL/glob, 0 for webcam')
    opt = parser.parse_args()
    print_args(FILE.stem, opt)
    return opt


def main(opt):
    check_requirements(exclude=('tensorboard', 'thop'))
    bachelor.run(**vars(opt))


if __name__ == "__main__":
    opt = parse_opt()
    main(opt)