import argparse

from dependencies.bachelorOcr import all_high_school
    
    
def parse_opt():
    parser = argparse.ArgumentParser()
    parser.add_argument('--ocr', type=str, help='file/dir/URL/glob, 0 for webcam')
    # parser.add_argument('--uniName', type=str, help='file/dir/URL/glob, 0 for webcam')
    opt = parser.parse_args()
    return opt

def main(opt):
    all_high_school(**vars(opt))

if __name__ == "__main__":
    opt = parse_opt()
    main(opt)