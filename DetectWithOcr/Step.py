from email import header
from operator import index
from traceback import print_tb
from dependencies.utils.Stepjson import data_in_json
import glob
import cv2
import json
import os
import pandas as pd
import sys
import argparse
from pathlib import Path 

FILE = Path(__file__).resolve()
# DATA = PureWindowsPath
ROOT = FILE.parents[0] 
if str(ROOT) not in sys.path:
    sys.path.append(str(ROOT))  # add ROOT to PATH
ROOT = Path(os.path.relpath(ROOT, Path.cwd()))  # relative
# USed for acquire old result by proving exp unique IDs 
def all_high_school(
    Stepocr
    ):
    # print("what is path ",highocr)
    # print("what is file parent ",FILE.parents[0] )
    arr = []
    dic=dict()
    datalist = []
    for i in glob.glob(Stepocr + "/*"):
        # print("what is i ",i)
        path = i + '/'
        for j in glob.glob(path + "*.jpg"):
            arr.append(j)
    for array in arr:
        data =array.split("/")
        if data[-2] == "CAA" or data[-2] == "CGP":
            datalist.append(cv2.imread(array))
            # print("true in lenth ", len(data[-2]))
            if len(datalist) == 2:
                # print("what is length ", len(datalist))
                dic["CAA"] = datalist[0]
                dic["CGPA"] = datalist[1]
                continue
            else:
                dic[data[-2]]=cv2.imread(array)
            # print("true")
        else:
            dic[data[-2]]=cv2.imread(array) 
    # print("dictionary ", dic)
    # print("what are the keys in high_school_ocr.py ==== ", dic.keys())
    result = data_in_json(dic, Stepocr)
    # print('what is dict ',len(dic["CAA"]))
    with open("High_School_ocr.json", "w") as outfile:
        json.dump(result, outfile)
    dictionary = json.dumps(result)
    return dictionary
    
def parse_opt():
    parser = argparse.ArgumentParser()
    # parser.add_argument('--highocr', type=PureWindowsPath)
    parser.add_argument('--Stepocr', type=str, help='file/dir/URL/glob, 0 for webcam')
    opt = parser.parse_args()
    return opt

def main(opt):
    all_high_school(**vars(opt))

if __name__ == "__main__":
    opt = parse_opt()
    main(opt)


